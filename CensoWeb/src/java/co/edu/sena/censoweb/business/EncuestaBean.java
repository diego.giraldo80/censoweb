/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuesta;
import co.edu.sena.censoweb.persistence.IEncuestaDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class EncuestaBean implements EncuestaBeanLocal {

    @EJB
    private IEncuestaDAO encuestaDAO;

    public void validate(Encuesta encuesta) throws Exception {
        if (encuesta == null) {
            throw new Exception("La encuesta es nulo");
        }

        if (encuesta.getFecha() == null) {
            throw new Exception("La fecha es obligatoria");
        }
        if (encuesta.getIdEncuestador().getCedula() == null) {
            throw new Exception("El encuestador es obligatorio");
        }
        if (encuesta.getIdPredio().getIdPredio() == null) {
            throw new Exception("El predio es obligatorio");
        }
        if (encuesta.getIdServicio().getIdServicio() == null) {
            throw new Exception("El servicio es obligatorio");
        }
        if (encuesta.getIdSuscriptor().getDocumento() == null) {
            throw new Exception("El suscriptor es obligatorio");
        }
    }

    @Override
    public void insert(Encuesta encuesta) throws Exception {
        validate(encuesta);
        encuestaDAO.insert(encuesta);
    }

    @Override
    public void update(Encuesta encuesta) throws Exception {
        validate(encuesta);
        Encuesta oldEncuesta = encuestaDAO.findbyid(encuesta.getNumeroFormulario());
        if (oldEncuesta == null) {
            throw new Exception("No existe la encuesta");
        }
        //merge
        oldEncuesta.setFecha(encuesta.getFecha());
        oldEncuesta.setIdEncuestador(encuesta.getIdEncuestador());
        oldEncuesta.setIdPredio(encuesta.getIdPredio());
        oldEncuesta.setIdServicio(encuesta.getIdServicio());
        oldEncuesta.setIdSuscriptor(encuesta.getIdSuscriptor());
        encuestaDAO.update(encuesta);
    }

    @Override
    public void delete(Encuesta encuesta) throws Exception {
        validate(encuesta);
        Encuesta oldEncuesta = encuestaDAO.findbyid(encuesta.getNumeroFormulario());
        if (oldEncuesta != null) {
            throw new Exception("Ya existe una encuesta con el mismo id");
        }
        encuestaDAO.delete(encuesta);
    }

    @Override
    public Encuesta findbyid(Integer numero_formulario) throws Exception {
        return encuestaDAO.findbyid(numero_formulario);
    }

    @Override
    public List<Encuesta> findAll() throws Exception {
        return encuestaDAO.findAll();
    }
}
