/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuesta;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface EncuestaBeanLocal {

    public void insert(Encuesta encuesta) throws Exception;

    public void update(Encuesta encuesta) throws Exception;

    public void delete(Encuesta encuesta) throws Exception;

    public Encuesta findbyid(Integer numero_formulario) throws Exception;

    public List<Encuesta> findAll() throws Exception;
}
