/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuestador;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface EncuestadorBeanLocal {
    public void insert(Encuestador encuestador)throws Exception;
    public void update(Encuestador encuestador)throws Exception;
    public void delete(Encuestador encuestador)throws Exception;
    public Encuestador findbyid(Long cedula) throws Exception;
    public List<Encuestador> findAll() throws Exception;
}
