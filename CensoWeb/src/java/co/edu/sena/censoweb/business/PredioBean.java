/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Predio;
import co.edu.sena.censoweb.persistence.IPredioDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class PredioBean implements PredioBeanLocal {

    @EJB
    private IPredioDAO predioDAO;

    public void validate(Predio predio) throws Exception {
        if (predio == null) {
            throw new Exception("El predio es nulo");
        }
        if (predio.getAbreviatura().isEmpty()) {
            throw new Exception("La abreviatura es obligatoria");
        }
        if (predio.getBarrio().isEmpty()) {
            throw new Exception("El barrio es obligatorio");
        }
        if (predio.getPrimerNumero() == null) {
            throw new Exception("El tercer numero es obligatorio");
        }
        if (predio.getSegundoNumero() == null) {
            throw new Exception("El segundo numero es obligatorio");
        }
        if (predio.getTercerNumero() == null) {
            throw new Exception("El tercer numero es obligatorio");
        }

    }

    @Override
    public void insert(Predio predio) throws Exception {
        validate(predio);
        predioDAO.insert(predio);
    }

    @Override
    public void update(Predio predio) throws Exception {
        validate(predio);
        Predio oldPredio = predioDAO.findbyid(predio.getIdPredio());
        if (oldPredio == null) {
            throw new Exception("No existe el predio");
        }
        //merge
        oldPredio.setAbreviatura(predio.getAbreviatura());
        oldPredio.setBarrio(predio.getBarrio());
        oldPredio.setComplemento(predio.getComplemento());
        oldPredio.setIdUso(predio.getIdUso());
        oldPredio.setNumeroOcupantes(predio.getNumeroOcupantes());
        oldPredio.setNumeroPisos(predio.getNumeroPisos());
        oldPredio.setPrimerNumero(predio.getPrimerNumero());
        oldPredio.setSegundoNumero(predio.getSegundoNumero());
        oldPredio.setTercerNumero(predio.getTercerNumero());
        predioDAO.update(predio);
    }

    @Override
    public void delete(Predio predio) throws Exception {
        validate(predio);
        Predio oldPredio = predioDAO.findbyid(predio.getIdPredio());
        if (oldPredio != null) {
            throw new Exception("No existe ese predio");
        }
        predioDAO.delete(predio);
    }

    @Override
    public Predio findbyid(Integer id_predio) throws Exception {
        return predioDAO.findbyid(id_predio);
    }

    @Override
    public List<Predio> findAll() throws Exception {
        return predioDAO.findAll();
    }

}
