/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Servicio;
import co.edu.sena.censoweb.persistence.IServiciosDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ServicioBean implements ServicioBeanLocal {

    @EJB
    private IServiciosDAO serviciosDAO;

    public void validate(Servicio servicio) throws Exception {
        if (servicio == null) {
            throw new Exception("El servicio es nulo");
        }
        if (servicio.getAcueducto() == null) {
            throw new Exception("El acueducto es obligatorio");
        }
        if (servicio.getAlcantarillado() == null) {
            throw new Exception("El alcantarillado es obligatorio");
        }
    }

    @Override
    public void insert(Servicio servicio) throws Exception {
        validate(servicio);
        serviciosDAO.insert(servicio);
    }
    
    @Override
    public void update(Servicio servicio) throws Exception {
        validate(servicio);
        Servicio oldServicios = serviciosDAO.findbyid(servicio.getIdServicio());
        if (oldServicios == null) {
            throw new Exception("No existe el encuestador");
        }
        //merge
        oldServicios.setAcueducto(servicio.getAcueducto());
        oldServicios.setAlcantarillado(servicio.getAlcantarillado());
        oldServicios.setLectura(servicio.getLectura());
        oldServicios.setMarcaMedidor(servicio.getMarcaMedidor());
        oldServicios.setSerieMedidor(servicio.getSerieMedidor());
    }
    
    @Override
    public void delete(Servicio servicio) throws Exception {
        validate(servicio);
        Servicio oldServicios = serviciosDAO.findbyid(servicio.getIdServicio());
        if (oldServicios != null) {
            throw new Exception("No existe ese encuestador");
        }
        serviciosDAO.delete(servicio);
    }
    
    @Override
    public Servicio findbyid(Integer idServicio) throws Exception {
        return serviciosDAO.findbyid(idServicio);
    }
    
    @Override
    public List<Servicio> findAll() throws Exception {
        return serviciosDAO.findAll();
    }
    
}
