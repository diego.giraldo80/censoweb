/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Suscriptor;
import co.edu.sena.censoweb.persistence.ISuscriptorDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class SuscriptorBean implements SuscriptorBeanLocal {
    @EJB
    private ISuscriptorDAO suscriptorDAO;
    public void validate(Suscriptor suscriptor) throws Exception {
        if (suscriptor == null) {
            throw new Exception("El suscriptor es nulo");
        }
        if (suscriptor.getDocumento()== null) {
            throw new Exception("El documento es obligatorio");
        }
        if (suscriptor.getTipoDocumento()== null) {
            throw new Exception("El tipo de documento es obligatorio");
        }
        if (suscriptor.getPrimerNombre()== null) {
            throw new Exception("El nombre es obligatorio");
        }
        if (suscriptor.getPrimerApellido()== null) {
            throw new Exception("El apellido es obligatorio");
        }
    }
    @Override
    public void insert(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findbyid(suscriptor.getDocumento());
        if (oldSuscriptor != null) {
            throw new Exception("Ya existe ese suscriptor");
        }
        suscriptorDAO.insert(suscriptor);
    }

    @Override
    public void update(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findbyid(suscriptor.getDocumento());
        if (oldSuscriptor == null) {
            throw new Exception("No existe el suscriptor");
        }
        //merge
        oldSuscriptor.setEmail(suscriptor.getEmail());
        oldSuscriptor.setPrimerApellido(suscriptor.getPrimerApellido());
        oldSuscriptor.setPrimerNombre(suscriptor.getPrimerNombre());
        oldSuscriptor.setSegundoApellido(suscriptor.getSegundoApellido());
        oldSuscriptor.setSegundoNombre(suscriptor.getSegundoNombre());
        oldSuscriptor.setTelefono(suscriptor.getTelefono());
        oldSuscriptor.setTipoDocumento(suscriptor.getTipoDocumento());
    }

    @Override
    public void delete(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findbyid(suscriptor.getDocumento());
        if (oldSuscriptor != null) {
            throw new Exception("Ya existe ese suscriptor");
        }
        suscriptorDAO.delete(suscriptor);
    }

    @Override
    public Suscriptor findbyid(Long documento) throws Exception {
        return suscriptorDAO.findbyid(documento);
    }

    @Override
    public List<Suscriptor> findAll() throws Exception {
        return suscriptorDAO.findAll();
    }
    
}
