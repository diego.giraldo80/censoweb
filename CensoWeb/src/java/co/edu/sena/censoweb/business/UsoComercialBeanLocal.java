/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.UsoComercial;
import java.util.List;
import javax.ejb.Local;

/**
 * Fecha:11/07/2022
 * @author Aprendiz
 * Objetivo: Gestionar logica de negocio de uso comercial
 */
@Local
public interface UsoComercialBeanLocal {
    public void insert(UsoComercial usoComercial)throws Exception;
    public void update(UsoComercial usoComercial)throws Exception;
    public void delete(UsoComercial usoComercial)throws Exception;
    public UsoComercial findbyid(Integer idUso) throws Exception;
    public List<UsoComercial> findAll() throws Exception;
}
