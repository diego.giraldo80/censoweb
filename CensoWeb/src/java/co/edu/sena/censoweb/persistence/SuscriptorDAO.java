/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Suscriptor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class SuscriptorDAO implements ISuscriptorDAO{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(Suscriptor suscriptor) throws Exception {
        try {
            entityManager.persist(suscriptor);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Suscriptor suscriptor) throws Exception {
        try {
            entityManager.merge(suscriptor);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Suscriptor suscriptor) throws Exception {
        try {
            entityManager.remove(suscriptor);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Suscriptor findbyid(Long documento) throws Exception {
        try {
            return entityManager.find(Suscriptor.class, documento);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Suscriptor> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Suscriptor.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
