/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.view;

import co.edu.sena.censoweb.business.EncuestadorBeanLocal;
import co.edu.sena.censoweb.model.Encuestador;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aprendiz
 */
@Named(value = "encuestadorView")
@RequestScoped
public class EncuestadorView {

    private InputText txtCedula;
    private InputText txtNombre;
    private InputText txtTelefono;
    private CommandButton btnCrear;
    private CommandButton btnModificar;
    private CommandButton btnEliminar;
    private List<Encuestador> listEncuestadores = null;

    @EJB
    private EncuestadorBeanLocal encuestadorBean;

    /**
     * Creates a new instance of EncuestadorView
     */
    public EncuestadorView() {
    }

    public InputText getTxtCedula() {
        return txtCedula;
    }

    public void setTxtCedula(InputText txtCedula) {
        this.txtCedula = txtCedula;
    }

    public InputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(InputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public InputText getTxtTelefono() {
        return txtTelefono;
    }

    public void setTxtTelefono(InputText txtTelefono) {
        this.txtTelefono = txtTelefono;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<Encuestador> getListEncuestadores() {
        if (listEncuestadores == null) {
            try {
                listEncuestadores = encuestadorBean.findAll();
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
        return listEncuestadores;
    }

    public void setListEncuestador(List<Encuestador> listEncuestador) {
        this.listEncuestadores = listEncuestador;
    }
    
    public void onRowSelect(SelectEvent event){
        Encuestador encuestador = (Encuestador) event.getObject();
        txtCedula.setValue(encuestador.getCedula());
        txtNombre.setValue(encuestador.getNombre());
        txtTelefono.setValue(encuestador.getTelefono());
        btnCrear.setDisabled(true);
        btnEliminar.setDisabled(false);
        btnModificar.setDisabled(false);
    }
    
    public void clear() {
        txtCedula.setValue("");
        txtTelefono.setValue("");
        txtNombre.setValue("");
        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);
        listEncuestadores = null;
    }

    public void insert() {
        try {
            Encuestador encuestador = new Encuestador();
            encuestador.setCedula(Long.parseLong(txtCedula.getValue().toString()));
            encuestador.setNombre(txtNombre.getValue().toString());
            encuestador.setTelefono(txtTelefono.getValue().toString());
            encuestadorBean.insert(encuestador);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Encuestador creado exitosamente"));
            clear();
        }catch (NumberFormatException e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "La cedula debe ser un numero"));
        }
         catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    public void update() {
        try {
            Encuestador encuestador = new Encuestador();
            encuestador.setCedula(Long.parseLong(txtCedula.getValue().toString()));
            encuestador.setNombre(txtNombre.getValue().toString());
            encuestador.setTelefono(txtTelefono.getValue().toString());
            encuestadorBean.update(encuestador);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Encuestador modificado exitosamente"));
            clear();
        }catch (NumberFormatException e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "La cedula debe ser un numero"));
        }
         catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    public void delete(){
        try {
            Encuestador encuestador = new Encuestador();
            encuestador.setCedula(Long.parseLong(txtCedula.getValue().toString()));
            encuestadorBean.delete(encuestador);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Encuestador eliminado exitosamente"));
            clear();
        }catch (NumberFormatException e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "La cedula debe ser un numero"));
        }
         catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    public void onDeleteConfirm(ActionEvent actionEvent){
        delete();
    }
    public void txtCedulaListener(){
        try {
            Encuestador encuestador = encuestadorBean.findbyid(Long.parseLong(txtCedula.getValue().toString()));
            if (encuestador == null) {
                txtNombre.setValue("");
                txtTelefono.setValue("");
                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso", "Encuestador no registrado"));
            }else{
                txtNombre.setValue(encuestador.getNombre());
                txtTelefono.setValue(encuestador.getTelefono());
                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
            }
        } catch (NumberFormatException e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "La cedula debe ser un numero"));
        }
         catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
}
